import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Feed } from '../../models/feed';
import { FeedsProvider } from '../../providers/feeds/feeds';

@Component({
  selector: 'page-feeds',
  templateUrl: 'feeds.html'
})
export class FeedsPage implements OnInit {

  feeds: Feed[];
  feedsProvider : FeedsProvider;

  constructor(public navCtrl: NavController) {
    this.feedsProvider = new FeedsProvider();
  }

  ngOnInit(){
    this.feedsProvider.getFeeds((feeds : Feed[], err) => {
      if (!err) {
        this.feeds = feeds;
      }
    });
  }

  openFeed(item: Feed) {
    this.navCtrl.push('FeeditemsPage', {feed:item.name});
  }

  unsubscribe(feed : Feed) {
    this.feedsProvider.unsubscribe(feed);
    this.feeds = this.feeds.filter(singleFeed => 
      singleFeed.url !== feed.url
    );
  }

}
