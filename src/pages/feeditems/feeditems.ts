import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, ToastController, NavParams} from 'ionic-angular';
import { Feeditem } from '../../models/feeditem';
import { Storage } from '@ionic/storage';
import { FeeditemsProvider } from '../../providers/feeditems/feeditems';
import { ArticlesProvider } from '../../providers/articles/articles';

@IonicPage()
@Component({
  selector: 'page-feeditems',
  templateUrl: 'feeditems.html'
})

export class FeeditemsPage implements OnInit {

  feedname : String;
  feeditemsProvider : FeeditemsProvider;
  feeditems : Feeditem[];
  articlesProvider : ArticlesProvider;
  storage : Storage;

  constructor(public navCtrl: NavController, navParams: NavParams, public storageParam: Storage, private toastCtrl: ToastController) {
    this.feedname = navParams.get('feed');
    this.articlesProvider = new ArticlesProvider();
    this.storage = storageParam;
    this.feeditemsProvider = new FeeditemsProvider();
  }

  ngOnInit(){
    this.feeditemsProvider.getUnreadFeeditemsByFeed(this.feedname, (feeditems) => {
      this.feeditems = feeditems;
    });
  }

  openArticle(item: Feeditem) {
    this.navCtrl.push('ArticlePage', {item:item, saved:false});
    this.feeditems.forEach((feeditem) => {
      if(feeditem.article_url === item.article_url) {
        feeditem.shown = true;
      }
    })
  }

  upvoteArticle(item: Feeditem) {
    this.articlesProvider.upvoteArticle(item);
    this.createToast('Article upvoted');
  }

  downvoteArticle(item: Feeditem) {
    this.articlesProvider.downvoteArticle(item);
    this.createToast('Article downvoted');
  }

  bookmarkArticle(item: Feeditem) {
    this.articlesProvider.saveArticle(item);
    // false is for don't mark as read
    this.articlesProvider.getArticle(item, false, (article, err) => {
      if(!err) {
        this.createToast('Article saved');
        // Saving the article in the LocalStorage
        this.storage.set(item.article_url, JSON.stringify(article));
      }
    });
  }

  createToast(text : string) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500
    });
    toast.present();
  }
}