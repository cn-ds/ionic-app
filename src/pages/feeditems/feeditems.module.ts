import { FeeditemsPage } from './feeditems';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
    declarations: [
      FeeditemsPage
    ],
    imports: [
      IonicPageModule.forChild(FeeditemsPage)
    ],
    exports: [
      FeeditemsPage
    ]
  })
  export class FeeditemsPageModule {}