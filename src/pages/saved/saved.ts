import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Feeditem } from '../../models/feeditem';
import { Config } from '../../shared/config';
import axios from 'axios';

@Component({
  selector: 'page-saved',
  templateUrl: 'saved.html'
})
export class SavedPage {

  feeditems: Feeditem[];

  constructor(public navCtrl: NavController) {
    axios.get(Config.API_ENDPOINT + '/saved/feeditems',
    {
      headers: {'x-access-token': Config.token}
    })
    .then((response) => {
      this.parseFeeditems(response.data);
    })
  }

  openArticle(item: Feeditem) {
    this.navCtrl.push('ArticlePage', {item:item, saved:true});
    this.feeditems.forEach((feeditem) => {
      if(feeditem.article_url === item.article_url) {
        feeditem.shown = true;
      }
    })
  }

  doRefresh(refresher) {
    axios.get(Config.API_ENDPOINT + '/saved/feeditems',
    {
      headers: {'x-access-token': Config.token}
    })
    .then((response) => {
      this.parseFeeditems(response.data);
    })
    refresher.complete();
  }

  unsaveArticle(item: Feeditem) {
    axios({
      method: 'POST',
      url: Config.API_ENDPOINT + '/article/unsave?url=' + item.article_url,
      headers: {'x-access-token': Config.token}
    });
    this.feeditems = this.feeditems.filter(
      fi => fi.article_url !== item.article_url
    );
  }

  parseFeeditems(jsonFeedArray) {
    this.feeditems = [];
    jsonFeedArray.forEach(elt => {
      this.feeditems.push(new Feeditem(
        elt.article_url,
        elt.title,
        elt.feed_parent,
        elt.author,
        elt.description,
        elt.date,
        elt.summary,
        elt.read,
        false,
        0
      ));
    });
  }

}
