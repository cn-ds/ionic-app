import { Component } from '@angular/core';

import { SavedPage } from '../saved/saved';
import { HomePage } from '../home/home';
import { OfflinePage } from '../offline/offline';
import { ParametersPage } from '../parameters/parameters';
import { FeedsPage } from '../feeds/feeds';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = SavedPage;
  tab3Root = ParametersPage;
  tab4Root = OfflinePage;
  tab5Root = FeedsPage;

  constructor() {

  }
}
