import { Component, OnInit } from '@angular/core';
import { NavController, ToastController} from 'ionic-angular';
import { Feeditem } from '../../models/feeditem';
import { Storage } from '@ionic/storage';
import { Util } from '../../shared/util';
import { FeeditemsProvider } from '../../providers/feeditems/feeditems';
import { ArticlesProvider } from '../../providers/articles/articles';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  feeditemsByScore: Feeditem[];
  feeditemsByDate: Feeditem[];
  lastDate: number;
  minScore : number;
  storage : Storage;
  shownFeeditem : String[];
  feeditemsProvider : FeeditemsProvider;
  articlesProvider : ArticlesProvider;
  ordering : String;
  SCORE : String = 'SCORE';
  DATE : String = 'DATE';
  feedItems : {SCORE : Feeditem[], DATE : Feeditem[]} = {'SCORE':this.feeditemsByScore,'DATE':this.feeditemsByDate};

  constructor(public navCtrl: NavController, public storageParam: Storage, private toastCtrl: ToastController) {
    this.lastDate = Number.MAX_SAFE_INTEGER;
    this.storage = storageParam;
    this.shownFeeditem = [];
    this.feeditemsProvider = new FeeditemsProvider();
    this.articlesProvider = new ArticlesProvider();
    this.ordering = this.SCORE;
  }

  ngOnInit(){
    this.storage.get('username').then((username) => {
      if (username !== null) {
        this.storage.get('password').then((password) => {
          Util.connect(username, password, this.storage).then(() => {
            this.feeditemsProvider.getUnreadFeeditems((feeditems) => {
              this.feeditemsByDate = feeditems;
              this.feedItems[this.DATE.toString()] = feeditems;

            });
            this.feeditemsProvider.getUnreadFeeditemsByScore((feeditems) => {
              this.feeditemsByScore = feeditems;
              this.feedItems[this.SCORE.toString()] = feeditems;
            });
          });
        });
      } else {
        this.feeditemsByDate = this.feeditemsProvider.getFakeFeeditems();
        this.feeditemsByScore = this.feeditemsProvider.getFakeFeeditems();
      }
    });
  }
  
  getLastDateAndShownFeedByDate() {
    this.shownFeeditem = [];
    this.feeditemsByDate.forEach((item) => {
      if(item.date < this.lastDate) {
        this.lastDate = item.date;
      }
      this.shownFeeditem.push(item.article_url);
    });
  }

  getLastDateAndShownFeedByScore() {
    this.shownFeeditem = [];
    this.feeditemsByScore.forEach((item) => {
      if(item.score < this.minScore) {
        this.minScore = item.score;
      }
      this.shownFeeditem.push(item.article_url);
    });
  }

  openArticle(item: Feeditem) {
    this.navCtrl.push('ArticlePage', {item:item, saved:false});
    this.feedItems[this.ordering.toString()].forEach((feeditem) => {
      if(feeditem.article_url === item.article_url) {
        feeditem.shown = true;
      }
    })
  }

  doRefresh(refresher) {
    if (this.ordering === this.DATE) {
      this.getLastDateAndShownFeedByDate();
      this.feeditemsByDate = [];
      this.feeditemsProvider.getUnreadFeeditems((feeditems) => {
        this.feeditemsByDate = feeditems;
        this.feedItems[this.DATE.toString()] = feeditems;
        refresher.complete();
        this.feeditemsProvider.markAsShown(this.shownFeeditem, () => {});
      }, this.lastDate);
    } else {
      this.getLastDateAndShownFeedByScore();
      this.feeditemsProvider.markAsShown(this.shownFeeditem, () => {
        this.feeditemsProvider.getUnreadFeeditemsByScore((feeditems) => {
          this.feeditemsByScore = feeditems;
          this.feedItems[this.SCORE.toString()] = feeditems;
          refresher.complete();
        });
      });     
    }
  }

  upvoteArticle(item: Feeditem) {
    this.articlesProvider.upvoteArticle(item);
    this.createToast('Article upvoted');
  }

  downvoteArticle(item: Feeditem) {
    this.articlesProvider.downvoteArticle(item);
    this.createToast('Article downvoted');
  }

  bookmarkArticle(item: Feeditem) {
    this.articlesProvider.saveArticle(item);
    // false is for don't mark as read
    this.articlesProvider.getArticle(item, false, (article, err) => {
      if(!err) {
        this.createToast('Article saved');
        // Saving the article in the LocalStorage
        this.storage.set(item.article_url, JSON.stringify(article));
      }
    });
  }

  createToast(text : string) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500
    });
    toast.present();
  }
}
