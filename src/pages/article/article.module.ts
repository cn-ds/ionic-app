import { ArticlePage } from './article';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
    declarations: [
      ArticlePage
    ],
    imports: [
      IonicPageModule.forChild(ArticlePage)
    ],
    exports: [
      ArticlePage
    ]
  })
  export class ArticlePageModule {}