import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { Clipboard } from '@ionic-native/clipboard';
import { IonicPage, Content, NavParams, NavController, ToastController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Article } from '../../models/article';
import { Storage } from '@ionic/storage';
import { Feeditem } from '../../models/feeditem';
import { ArticlesProvider } from '../../providers/articles/articles';
import { Config } from '../../shared/config';

@IonicPage()
@Component({
  selector: 'page-article',
  templateUrl: 'article.html'
})
export class ArticlePage implements OnInit {

  @ViewChild(Content) content: Content;

  article : Article;
  item : Feeditem;
  saved : boolean;
  progress : Number;
  clipboard : Clipboard;
  toastCtrl : ToastController;
  browser : InAppBrowser;
  articlesProvider : ArticlesProvider;

  constructor(public navCtrl: NavController, toastCtrl: ToastController, navParams: NavParams, iab: InAppBrowser, private storage: Storage, private zone: NgZone) {
    this.saved = navParams.get('saved');
    this.item = navParams.get('item');
    this.article = new Article('Waiting...', 'Wait', false, false);
    this.toastCtrl = toastCtrl;
    this.browser = iab;
    this.progress = 0;
    this.articlesProvider = new ArticlesProvider();
  }

  ngOnInit(){
    console.log("ngOnInit");

    if (this !== null && this.saved) {
      this.storage.get(this.item.article_url).then((articleParsed) => {
        if (articleParsed !== null) {
          this.article = JSON.parse(articleParsed)
        } else {
          this.retrieveArticle();
        }
      });
    } else {
      this.retrieveArticle();
    }
  }

  updateProgressBar() {
    this.zone.run(() => {
      var element = document.getElementById("content-size");
      var a = this.content.scrollTop + this.content.contentHeight;
      var b = element.offsetHeight;
      this.progress = (a / b) * 100;
    });
  }

  retrieveArticle() {
    this.articlesProvider.getArticle(this.item, true, (article, err) => {
      if (!err) {
        this.article = article;
        if (this.saved) {
          this.storage.set(this.item.article_url, JSON.stringify(this.article));
        }
      }
    });
  }

  bookmarkArticle(article: Article) {
    this.articlesProvider.saveArticle(this.item);
    this.article.saved = true;
    this.createToast('Article saved');
    this.storage.set(this.item.article_url, JSON.stringify(article));
  }

  openInBrowser(article: Article) {
    this.browser.create(this.item.article_url, '_system', 'location=yes');
  }

  copyUrl(article: Article) {
    this.article.shared = true;
    this.clipboard = new Clipboard();
    this.clipboard.copy(Config.SHARE_URL + encodeURIComponent(this.item.article_url));
    this.createToast('Link copied');
  }

  createToast(text : string) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500
    });
    toast.present();
  }
}
