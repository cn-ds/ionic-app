import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { SavedPage } from '../pages/saved/saved';
import { ParametersPage } from '../pages/parameters/parameters';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { OfflinePage } from '../pages/offline/offline';
import { FeedsPage } from '../pages/feeds/feeds';

import { ArticlePageModule } from '../pages/article/article.module';
import { FeeditemsPageModule } from '../pages/feeditems/feeditems.module'; 

import { SettingsProvider } from './../providers/settings/settings';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@NgModule({
  declarations: [
    MyApp,
    SavedPage,
    ParametersPage,
    OfflinePage,
    HomePage,
    FeedsPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    ArticlePageModule,
    FeeditemsPageModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SavedPage,
    HomePage,
    OfflinePage,
    ParametersPage,
    FeedsPage,
    TabsPage
  ],
  providers: [
    InAppBrowser,
    StatusBar,
    SplashScreen,
    SettingsProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
