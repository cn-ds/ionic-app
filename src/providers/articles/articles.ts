import { Article } from '../../models/article';
import { ApiProvider } from '../api/api';
import { Feeditem } from '../../models/feeditem';

export class ArticlesProvider {

  api : ApiProvider;

  constructor() {
    this.api = new ApiProvider();
  }

  parseArticle(article) : Article {
    return new Article(article.title, article.content, false, false);
  }

  getArticle(feeditem : Feeditem, markAsRead : boolean, callback) {
    this.api.getJSON(`/article?url=${feeditem.article_url}`, true, (articleData, err) => {
      if(!err) {
        let article = this.parseArticle(articleData);
        callback(article, false);
        if(markAsRead) {
          this.api.post(`/article/read?url=${feeditem.article_url}`, {}, true, () => {});
        }
      } else {
        callback(null, true);
      }
    })
  }

  upvoteArticle(feeditem : Feeditem) {
    this.api.post(`/article/interesting?url=${feeditem.article_url}`, {}, true, () => {});
  }

  downvoteArticle(feeditem : Feeditem) {
    this.api.post(`/article/boring?url=${feeditem.article_url}`, {}, true, () => {});
  }

  saveArticle(feeditem : Feeditem) {
    this.api.post(`/article/save?url=${feeditem.article_url}`, {}, true, () => {});
  }
}