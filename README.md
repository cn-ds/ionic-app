# Feather Brief Ionic App

## Description

For a full description of the project you should go [here](https://cn-ds.gitlab.io/blog/rex/2018/08/11/feather-brief-overview.html)

## Installation

```` shell
nvm use 6
ionic serve
````

## Reminder

* Debugging in Chrome is easier

## Build

```` shell
# First line is personnal conf
export ANDROID_HOME=$ANDROID_HOME/Sdk
ionic cordova build --debug android
````
## Generate resources

```` shell
ionic cordova resources
````

## Release

```` shell
ionic cordova build --release android
cd /home/constant/Projets/rss-panda-app/platforms/android/app/build/outputs/apk/release/
keytool -genkey -v -keystore feather-brief.keystore -alias feather-brief -keyalg RSA -keysize 2048 -validity 10000
xdg-open .
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore feather-brief.keystore app-release-unsigned.apk feather-brief
zipalign -v 4 app-release-unsigned.apk Feather-brief.apk
echo $ANDROID_HOME
/home/constant/Android/Sdk/build-tools/27.0.3/zipalign -v 4 app-release-unsigned.apk Feather-brief.apk
````
